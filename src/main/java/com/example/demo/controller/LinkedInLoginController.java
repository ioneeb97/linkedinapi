package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@RestController
@RequestMapping(value = "/linkedin")
@CrossOrigin(value = "http://localhost:4200")
public class LinkedInLoginController {
//    @Autowired
//    private RestTemplate restTemplate;

    @PostMapping(value = "/login")
    public ResponseEntity fetchToken(@RequestBody HashMap<String,String> object){

        String redirectUrl = "http://localhost:4200/login";
        String linkedinUrl = "https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code="+ object.get("accessCode") +"&redirect_uri="+redirectUrl + "&client_id="+ object.get("clientId") + "&client_secret="+ object.get("clientSecret");

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response;
        response = restTemplate.getForEntity(linkedinUrl, String.class);

         return ResponseEntity.status(200).body(response.getBody());
    }

    @PostMapping("/profile")
    public ResponseEntity getProfile(@RequestBody HashMap<String,String> object){
        String url = "https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))";
        String encodedAuth = "Bearer " + object.get("accessToken");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", encodedAuth);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response;
        response = restTemplate.exchange(url,HttpMethod.GET, entity,String.class);

        return ResponseEntity.status(200).body(response.getBody());
    }

    @PostMapping("/get/email-address")
    public ResponseEntity getEmailAddress(@RequestBody HashMap<String,String> object){
        String url = "https://api.linkedin.com/v2/clientAwareMemberHandles?q=members&projection=(elements*(primary,type,handle~))";
        String encodedAuth = "Bearer " + object.get("accessToken");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", encodedAuth);

        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response;
        response = restTemplate.exchange(url,HttpMethod.GET, entity,String.class);

        return ResponseEntity.status(200).body(response.getBody());
    }
}
